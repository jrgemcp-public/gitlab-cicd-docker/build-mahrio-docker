ARG NODE_VERSION

FROM node:$NODE_VERSION

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update

RUN apt install git zip curl jq --yes

RUN npm install @nrwl/cli --global

RUN npm install ts-node --global

RUN ts-node --version
