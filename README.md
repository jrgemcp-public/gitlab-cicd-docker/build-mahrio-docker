# build-mahrio-docker

This repo contains the Docker image needed for building MAHRIO related assets

```
image: registry.gitlab.com/jrgemcp-public/gitlab-cicd-docker/build-mahrio-docker:latest
```
